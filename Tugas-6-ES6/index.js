//SOAL NOMER 1
//============================================================//

const keliling = (panjang, lebar) => {
    let kelilingPersegi = 2 * (panjang + lebar)
    return kelilingPersegi
    
}

console.log('Keliling Persegi Panjang = ' + keliling(12, 6))


const luas = (panjang, lebar) => {
    let luasPersegi = panjang * lebar
    return luasPersegi
    
}

console.log('Luas Persegi Panjang = ' + luas(12, 6))

//SOAL NOMER 2
//============================================================//
const newFunction = (firstName, lastName) => {
    return {
      firstName,
      lastName,
      fullName: function(){
        console.log(firstName + " " + lastName)
      }
    }
  }
   
  //Driver Code 
  newFunction("William", "Imoh").fullName() 

//SOAL NOMER 3
//============================================================//

const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
}

//ES5
// const firstName = newObject.firstName;
// const lastName = newObject.lastName;
// const address = newObject.address;
// const hobby = newObject.hobby;

//ES6
const {firstName, lastName, address, hobby} = newObject

// Driver code
console.log(firstName, lastName, address, hobby)


//SOAL NOMER 4
//============================================================// 

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

//ES5 CONCATENATION
const combined = west.concat(east)

//ES6 SPREAD OPERATOR
const newArray = [...west, ...east]
//Driver Code
console.log(newArray)

//SOAL NOMER 5
//============================================================//

const planet = "earth" 
const view = "glass" 

//ES5
var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet 

//ES5
const after = `Lorem ${planet} dolor sit amet, consectetur adipiscing elit ${view}`
console.log(after)




