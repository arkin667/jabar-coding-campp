//SOAL 1 MENRGURUTKAN ARRAY
//JAWABAN

var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];

daftarHewan.sort()
console.log(daftarHewan)

//SOAL 2 MENRGURUTKAN ARRAY
//JAWABAN

function introduce(data){
    return 'Nama saya ' + data.name + ', umur saya ' + data.age + ' tahun, alamat saya di ' + data.address + ', dan saya punya hobby yaitu ' + data.hobby
}

var data = {name : "John" , 
age : 30 , 
address : "Jalan Pelesiran" , 
hobby : "Gaming" }
 
var perkenalan = introduce(data)
console.log(perkenalan) // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming" 


//SOAL 3 MENRGURUTKAN ARRAY
//JAWABAN
const vokal = ["a", "e", "i", "o", "u"]

function hitung_huruf_vokal(str) {
    let count = 0;
    var lowerCase = str.toLowerCase()
    for(let i = 0; i < lowerCase.length; i++){
        for(let j = 0; j < vokal.length; j++){
            if(lowerCase[i] == vokal[j]){
                count++
            }
        }
    }
     return count
}

var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2) // 3 2

//SOAL 3 MENRGURUTKAN ARRAY
//JAWABAN

function hitung(n){
    var nilai = -4;
    for (let i = 0; i <= n; i++) {
        nilai += 2;
    }
    return nilai
}

console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8


