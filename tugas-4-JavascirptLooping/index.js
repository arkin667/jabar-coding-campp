//SOAL NOMOR 1
//KONDISI / FORLOOP

var nilai = 65;
if (nilai >= 85) {
  console.log("indeksnya A");
} else if (nilai >= 75 && nilai < 85) {
  console.log("indeksnya B");
} else if (nilai >= 65 && nilai < 75) {
  console.log("indeksnya C");
} else if (nilai >= 55 && nilai < 65) {
  console.log("indeksnya D");
} else if (nilai < 55) {
  console.log("indeksnya E");
}

//SOAL NOMOR 2
//

var tanggal = 27;
var bulan = 5;
var tahun = 1999;

switch (bulan) {
  case 1:
    bulan = 'Januari';
    break;
  case 2:
    bulan = "Februari";
    break;
  case 3:
    bulan = "Maret";
    break;
  case 4:
    bulan = "April";
    break;
  case 5:
    bulan = "Mei";
    break;
  case 6:
    bulan = "Juni";
    break;
  case 7:
    bulan = "July";
    break;
  case 8:
    bulan = "Agustus";
    break;
  case 9:
    bulan = "September";
    break;
  case 10:
    bulan = "Oktober";
    break;
  case 11:
    bulan = "November";
    break;
  case 12:
    bulan = "Desember";
    break;
}

var hasilBulan = bulan;
console.log(tanggal + hasilBulan + tahun);

//SOAL NOMOR 3
//PATERN

function segitiga(n) {
  let pola = "";
  for (let i = 1; i <= n; i++) {
    for (let j = 1; j <= i; j++) {
      pola += "#";
    }
    pola += "\n";
  }
  return pola;
}

console.log(segitiga(7));
// Rubah paramter sesuai yang diinginkan
// 3 & 7

//SOAL NOMOR 4
//
var str1 = "programming";
var str2 = "Javascript";
var str3 = 'VueJS'
function integer(m){
    for(let i = 1; i <= m; i++){
        if(i % 3 === 1){
            console.log(i +' - I Love ' + str1 )
        }else if(i % 3 === 2){
            console.log(i +' - I Love ' + str2)
        }else if(i % 1 === 0){
            console.log(i +' - I Love ' + str3)
            pola = ""
            if(i % 3 == 0){
                for (let j = 1; j <= i ; j++) {
                  pola += "=";
                }
              }
              console.log(pola)
        }
    }
    return
}
integer(9)
//ganti parameter sesuai ouput yang diinginkan 
// 3 9 10 